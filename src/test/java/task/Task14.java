package task;

public class Task14 {
    public static void main(String[] args) {

        System.out.println(silnia(10));
        System.out.println(silnia_rekurencyjna(10));
        silniaForEach(10);

    }

    private static int silnia(int n) {
        int score = 1;
        for (int i = 1; i <= n; i++)
            score *= i;
        return score;
    }


    private static int silnia_rekurencyjna(int i) {
        if (i == 0) {
            return 1;
        } else
            return (i * silnia_rekurencyjna(i - 1));
    }

    private static void silniaForEach(int b) {
        int n;
        int silnia = 1;
        while (b > 1) {
            silnia = silnia * b;
            b -= 1;
        }
        System.out.println("Silnia z podanej liczby to: " + silnia);


    }
}


